package com.task

import com.task.controllers.TestEntityController
import com.task.dto.TestEntityUpdateDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.Instant
import java.util.NoSuchElementException
import java.util.UUID.randomUUID

@SpringBootTest
class TestEntityControllerTests(@Autowired val controller: TestEntityController) {

	@Test
	fun postsNewEntity() {
		//given
		val postDto = TestEntityUpdateDto("1", randomUUID(), Instant.now(), randomUUID(), "valueNameTest")

		//when
		val resultDto = controller.post(postDto)

		//then
		assertThat(resultDto).isNotNull
		assertThat(resultDto.sortOrder).isEqualTo(postDto.sortOrder)
		assertThat(resultDto.documentId).isEqualTo(postDto.documentId)
		assertThat(resultDto.documentDate).isEqualTo(postDto.documentDate)
		assertThat(resultDto.dictionaryValueId).isEqualTo(postDto.dictionaryValueId)
		assertThat(resultDto.dictionaryValueName).isEqualTo(postDto.dictionaryValueName)
	}

	@Test
	fun getsPostedEntity() {
		//given
		val postDto = TestEntityUpdateDto("1", randomUUID(), Instant.now(), randomUUID(), "valueNameTest")

		//when
		val resultDto = controller.post(postDto)
		val getDto = controller.get(resultDto.id)

		//then
		assertThat(getDto).isNotNull
		assertThat(getDto.sortOrder).isEqualTo(postDto.sortOrder)
		assertThat(getDto.documentId).isEqualTo(postDto.documentId)
		assertThat(getDto.dictionaryValueId).isEqualTo(postDto.dictionaryValueId)
		assertThat(getDto.dictionaryValueName).isEqualTo(postDto.dictionaryValueName)
	}

	@Test
	fun updatesPostedEntity() {
		//given
		val postDto = TestEntityUpdateDto("1", randomUUID(), Instant.now(), randomUUID(), "valueNameTest")
		val patchDto = TestEntityUpdateDto("2", randomUUID(), Instant.now(), randomUUID(), "valueNameTest2")

		//when
		val resultDto = controller.post(postDto)
		val resultPatchDto = controller.patch(resultDto.id, patchDto)

		//then
		assertThat(resultPatchDto).isNotNull
		assertThat(resultPatchDto.sortOrder).isEqualTo(patchDto.sortOrder)
		assertThat(resultPatchDto.documentId).isEqualTo(patchDto.documentId)
		assertThat(resultPatchDto.dictionaryValueId).isEqualTo(patchDto.dictionaryValueId)
		assertThat(resultPatchDto.dictionaryValueName).isEqualTo(patchDto.dictionaryValueName)
	}

	@Test
	fun deletesPostedEntity() {
		//given
		val postDto = TestEntityUpdateDto("1", randomUUID(), Instant.now(), randomUUID(), "valueNameTest")

		//when
		val resultDto = controller.post(postDto)
		controller.delete(resultDto.id)

		//then
		assertThatThrownBy { controller.get(resultDto.id) }
			.isInstanceOf(NoSuchElementException::class.java)
	}

}
