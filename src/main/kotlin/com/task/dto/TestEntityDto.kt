package com.task.dto

import java.time.Instant
import java.util.*

data class TestEntityDto (
    val id: UUID,
    val sortOrder: String,
    val documentId: UUID?,
    val documentDate: Instant?,
    val dictionaryValueId: UUID?,
    val dictionaryValueName: String?
)