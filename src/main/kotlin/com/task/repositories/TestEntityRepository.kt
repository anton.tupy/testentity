package com.task.repositories

import com.task.model.TestEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface TestEntityRepository: JpaRepository<TestEntity, UUID> {
    @Query("from TestEntity order by sortOrder")
    fun listAllOrderBySortOrder(): List<TestEntity>
}
