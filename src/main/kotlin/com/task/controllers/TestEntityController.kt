package com.task.controllers

import com.task.dto.TestEntityDto
import com.task.dto.TestEntityUpdateDto
import com.task.mapping.TestEntityDtoMapping.toDto
import com.task.mapping.TestEntityDtoMapping.toEntity
import com.task.services.TestEntityService
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.UUID.randomUUID

@RestController
@RequestMapping("/test-entity")
class TestEntityController(private val service: TestEntityService) {

    @GetMapping("{id}")
    fun get(@PathVariable("id") id: UUID): TestEntityDto {
        return toDto(service.findById(id) ?: throw NoSuchElementException())
    }

    @GetMapping
    fun list(): List<TestEntityDto> {
        return service.list().map { toDto(it) }
    }

    @PostMapping
    fun post(@RequestBody dto: TestEntityUpdateDto): TestEntityDto {
        return toDto(service.save(toEntity(randomUUID(), dto)))
    }

    @PatchMapping("{id}")
    fun patch(@PathVariable("id") id: UUID, @RequestBody dto: TestEntityUpdateDto): TestEntityDto {
        return toDto(service.save(toEntity(id, dto)))
    }

    @DeleteMapping("{id}")
    fun delete(@PathVariable("id") id: UUID) {
        service.delete(id)
    }
}