package com.task

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories(basePackages = ["com.task.repositories"])
class TaskApplication

fun main(args: Array<String>) {
	runApplication<TaskApplication>(*args)
}
