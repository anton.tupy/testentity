package com.task.model

import java.time.Instant
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class TestEntity (
    @Id
    val id: UUID,
    val sortOrder: String,
    val documentId: UUID?,
    val documentDate: Instant?,
    val dictionaryValueId: UUID?,
    val dictionaryValueName: String?
)
