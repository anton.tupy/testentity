package com.task.services

import com.task.model.TestEntity
import com.task.repositories.TestEntityRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class TestEntityService(private val repository: TestEntityRepository) {

    fun findById(id: UUID): TestEntity? = repository.findById(id).orElse(null)

    fun list(): List<TestEntity> = repository.listAllOrderBySortOrder()

    fun save(entity: TestEntity): TestEntity = repository.save(entity)

    fun delete(id: UUID) {
        repository.deleteById(id)
    }

}
