package com.task.mapping

import com.task.dto.TestEntityDto
import com.task.dto.TestEntityUpdateDto
import com.task.model.TestEntity
import java.util.*

object TestEntityDtoMapping {
    fun toDto(entity: TestEntity): TestEntityDto =
        TestEntityDto(entity.id, entity.sortOrder, entity.documentId, entity.documentDate, entity.dictionaryValueId, entity.dictionaryValueName)

    fun toEntity(id: UUID, dto: TestEntityUpdateDto): TestEntity =
        TestEntity(id, dto.sortOrder, dto.documentId, dto.documentDate, dto.dictionaryValueId, dto.dictionaryValueName)
}
