FROM adoptopenjdk:16-jre-hotspot
COPY ./target/task-*.jar /application.jar
CMD ["java","-jar", "/application.jar"]
